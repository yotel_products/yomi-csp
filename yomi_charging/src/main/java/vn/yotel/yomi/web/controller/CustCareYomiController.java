package vn.yotel.yomi.web.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestParam;
import vn.yotel.commons.util.RestMessage;
import vn.yotel.commons.util.Util;
import vn.yotel.vbilling.jpa.*;
import vn.yotel.vbilling.model.*;
import vn.yotel.vbilling.repository.MtSmsRepo;
import vn.yotel.vbilling.service.*;
import vn.yotel.vbilling.util.ChargingCSPClient;
import vn.yotel.vbilling.util.TransferIsdn;
import vn.yotel.vbilling.util.Utils;
import vn.yotel.yomi.AppParams;
import vn.yotel.yomi.Constants;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

//import com.hazelcast.core.IMap;


@Controller
@RequestMapping(value = "/custcare/kpbt")
public class CustCareYomiController {

    private Logger LOG = LoggerFactory.getLogger(CustCareYomiController.class);

    private List<VasPackage> allVasPackages;

    private String MT_REG_SUCCESS1 = "";
    private String MT_REG_SUCCESS2 = "";
    private String MT_REG_AGAIN = "";
    private String MT_REG_INVALID_BALANCE = "";
    private String MT_CAN_SUCCESS = "";
    private String MT_CAN_FAILED = "";

    private String ERR_INVALID_BALANCE = "0001";
    private String ERR_REG_AGAIN = "0002";
    private String ERR_INVALID_PRODUCT_OR_PACKAGE = "0003";
    private String ERR_INVALID_ISDN = "0006";
    private String ERR_EXCEPTION = "9999";
    private String ERR = "0";

    @Autowired
    SubscriberService subscriberService;

    @Autowired
    VasPackageService vasPackageService;

    @Autowired
    CpGateService cpGateService;

//    @Autowired
//    MtSmsRepo mtSmsRepo;

    @Autowired
    BlacklistService blacklistService;

    @Autowired
    SmsService smsService;

    @Autowired
    XsPromotionSumService xsPromotionSumService;

    @Autowired
    XsPromotionLogService xsPromotionLogService;

    @Autowired
    SubsChargeLogService subsChargeLogService;

    @Autowired
    CdrLogService cdrLogService;

    @Resource
    private ConcurrentLinkedQueue<MTRequest> mtQueueToCSP;

    @Resource
    private Object mtQueueToCSPNotifier;

    @Resource
    private ChargingCSPClient chargingCSPClient;

    @RequestMapping(value = {"/cancel.html"}, method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public String ccSubsUnRegister(@RequestParam("request_id") String requestId, @RequestParam("msisdn") String isdn, @RequestParam("product_id") int productId, @RequestParam("package_code") String packageName, HttpServletRequest request) {
        RestMessage wapResult = RestMessage.RestMessageBuilder.SUCCESS();
        String smsMessage = "";
        try {
            String msisdn = Util.normalizeMsIsdn(isdn);
            String serviceCode = AppParams.SHORT_CODE;
            String packageCode = packageName;
            String commandCode = "HUY";
            String sourceCode = "CP";
            VasPackage vasPackage = vasPackageService.findByName(packageCode);
            if (vasPackage == null) {
                LOG.warn("Could not find corresponding packageCode: {}", packageCode);
                wapResult = RestMessage.RestMessageBuilder.FAIL(ERR, "Khong tin thay ma goi cuoc " + packageCode);
            } else {
                Subscriber subscriber = subscriberService.findByMsisdnAndPackageIdAndStatus(msisdn, vasPackage.getId(), 1);
                Date now = new Date();
                if (subscriber != null) {
                    subscriber.setStatus(0);
                    subscriber.setModifiedDate(now);
                    subscriber.setUnregisterDate(now);
                    this.subscriberService.update(subscriber);
                    //String command = Constants.CommandCode.CANCEL;
                    //ghi log cac tham so tren request gui ve
                    //this.subsChargeLogService.logSubsCharge(isdn, now, command, 0, vasPackage.getName(), "", "CP");
                }
            }

            String result = this.chargingCSPClient.receiverServiceReq(serviceCode, msisdn, commandCode, packageCode, sourceCode);

            this.LOG.info("serviceCode {}", serviceCode);
            this.LOG.info("msisdn {}", msisdn);
            this.LOG.info("commandCode {}", commandCode);
            this.LOG.info("packageCode {}", packageCode);
            this.LOG.info("sourceCode {}", sourceCode);
            this.LOG.info("receiverServiceReq: {}", result);

        } catch (Exception ex) {
            this.LOG.error("", ex);
            wapResult = RestMessage.RestMessageBuilder.FAIL(this.ERR_EXCEPTION, ex.getMessage());
        }
        Gson gson = (new GsonBuilder()).serializeNulls().excludeFieldsWithoutExposeAnnotation().create();
        return gson.toJson(wapResult);
    }

    /**
     * @param mtReq
     */
    private void sendSMS(MTRequest mtReq) {
        mtQueueToCSP.offer(mtReq);
        synchronized (mtQueueToCSPNotifier) {
            mtQueueToCSPNotifier.notifyAll();
        }
    }

    Gson GSON_ALL = new GsonBuilder().serializeNulls().setDateFormat("yyyy/MM/dd HH:mm:ss").create();

    @RequestMapping(value = "/mt_history.html", method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    String getMtHistory(
            @RequestParam("msisdn") String msisdn,
            @RequestParam("startdate") String startDate,
            @RequestParam("enddate") String endDate) {
        LOG.info("from: {}, to: {}", startDate, endDate);
        RestMessage resp = null;
        try {
            Date _fromDate = Util.BB_CC_SDF_yyyyMMdd.parse(startDate);
            Date _toDate = Util.BB_CC_SDF_yyyyMMdd.parse(endDate);
            String newIsdn = TransferIsdn.transferToParam(msisdn);
            List<MtHistory> listMt = smsService.getMtHistory(newIsdn, _fromDate, _toDate);
            LOG.info(listMt.toString());
            resp = RestMessage.RestMessageBuilder.SUCCESS();
            resp.setData(listMt);
        } catch (Exception e) {
            LOG.error("", e);
            resp = RestMessage.RestMessageBuilder.FAIL("001", e.getMessage());
        }
        return GSON_ALL.toJson(resp);
    }


    @RequestMapping(value = {"/blacklist/add.html"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    String addNumberToBlacklist(
            HttpServletRequest request,
            @RequestParam(value = "msisdn") String msisdn) {
        RestMessage resp = null;
        try {
            msisdn = Util.normalizeMsIsdn(msisdn);
//            IPRangeChecker.validateBB_IPS(request.getRemoteAddr());
            Blacklist result = blacklistService.addBlacklist(msisdn);
            if (result != null) {
                resp = RestMessage.RestMessageBuilder.FAIL("001", "Could not add this number to Blacklist");
            }
            resp = RestMessage.RestMessageBuilder.SUCCESS();
        } catch (Exception e) {
            resp = RestMessage.RestMessageBuilder.FAIL("9999", e.getMessage());
        }
        return GSON_ALL.toJson(resp);
    }

    @RequestMapping(value = {"/blacklist/remove.html"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    String removeNumberFromBlacklist(
            HttpServletRequest request,
            @RequestParam(value = "msisdn") String msisdn) {
        RestMessage resp = null;
        try {
            msisdn = Util.normalizeMsIsdn(msisdn);
//            IPRangeChecker.validateBB_IPS(request.getRemoteAddr());
            Blacklist result = blacklistService.removeBlacklist(msisdn);
            if (result != null) {
                resp = RestMessage.RestMessageBuilder.FAIL("001", "Could not remove this number to Blacklist");
            }
            resp = RestMessage.RestMessageBuilder.SUCCESS();
        } catch (Exception e) {
            resp = RestMessage.RestMessageBuilder.FAIL("9999", e.getMessage());
        }
        return GSON_ALL.toJson(resp);
    }

    @RequestMapping(value = {"/blacklist/list.html"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    String getBlacklist(
            HttpServletRequest request,
            @RequestParam(value = "page", required = false, defaultValue = "0") int page,
            @RequestParam(value = "pageSize", required = false, defaultValue = "10000") int pageSize) {
        RestMessage resp = null;
        try {
//            IPRangeChecker.validateBB_IPS(request.getRemoteAddr());
            List<Blacklist> subs = blacklistService.getAllBlackListSubs();
            resp = RestMessage.RestMessageBuilder.SUCCESS();
            resp.setData(subs);
        } catch (Exception e) {
            resp = RestMessage.RestMessageBuilder.FAIL("9999", e.getMessage());
        }
        return GSON_ALL.toJson(resp);
    }

    @RequestMapping(value = {"/blacklist/search.html"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    String getSearchBlacklist(
            HttpServletRequest request,
            @RequestParam(value = "msisdn", required = true, defaultValue = "") String msisdn) {
        RestMessage resp = null;
        try {
            msisdn = Util.normalizeMsIsdn(msisdn);
            String temp = TransferIsdn.transferToNativeSQL(msisdn);
//            IPRangeChecker.validateBB_IPS(request.getRemoteAddr());
            Blacklist subs = blacklistService.findByMsisdn(temp);
            if (subs != null && subs.getStatus()) {
                resp = RestMessage.RestMessageBuilder.SUCCESS();
                resp.setData(subs);
            } else {
                resp = RestMessage.RestMessageBuilder.FAIL("002", "Could not find this number in blacklist: " + msisdn);
            }
        } catch (Exception e) {
            resp = RestMessage.RestMessageBuilder.FAIL("9999", e.getMessage());
        }
        return GSON_ALL.toJson(resp);
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    public String missingParamterHandler(Exception exception) {
        LOG.error("", exception);
        return "/400"; /* view name of your erro jsp */
    }

    @RequestMapping(value = "/promotion_sum.html", method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    String getPromotionSum(
            @RequestParam("startdate") String startDate,
            @RequestParam("enddate") String endDate) {
        LOG.info("from: {}, to: {}", startDate, endDate);
        RestMessage resp = null;
        try {
            Date _fromDate = Util.BB_CC_SDF_yyyyMMdd.parse(startDate);
            Date _toDate = Util.BB_CC_SDF_yyyyMMdd.parse(endDate);
            List<XsPromotionSum> listPromotionSum = null;
            try {
                listPromotionSum = xsPromotionSumService.findAllByDate(_fromDate, _toDate);
            } catch (Exception ex) {
                LOG.error("", ex);
            }
            LOG.info(listPromotionSum.toString());
            resp = RestMessage.RestMessageBuilder.SUCCESS();
            resp.setData(listPromotionSum);
        } catch (Exception e) {
            LOG.error("", e);
            resp = RestMessage.RestMessageBuilder.FAIL("001", e.getMessage());
        }
        return GSON_ALL.toJson(resp);
    }

    @RequestMapping(value = "/promotion_log.html", method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    String getPromotionLog(
            @RequestParam("startdate") String startDate,
            @RequestParam("enddate") String endDate,
            @RequestParam("msisdn") String msisdn) {
        LOG.info("from: {}, to: {}", startDate, endDate);
        RestMessage resp = null;
        try {
            Date _fromDate = Util.BB_CC_SDF_yyyyMMdd.parse(startDate);
            Date _toDate = Util.BB_CC_SDF_yyyyMMdd.parse(endDate);
            List<XsPromotionLog> listPromotionLog = null;
            try {
                listPromotionLog = xsPromotionLogService.findAllByDate(_fromDate, _toDate, msisdn);
            } catch (Exception ex) {
                LOG.error("", ex);
            }
//            LOG.info(listPromotionLog.toString());
            resp = RestMessage.RestMessageBuilder.SUCCESS();
            resp.setData(listPromotionLog);
        } catch (Exception e) {
            LOG.error("", e);
            resp = RestMessage.RestMessageBuilder.FAIL("001", e.getMessage());
        }
        return GSON_ALL.toJson(resp);
    }

    @RequestMapping(value = "sub-info.html", method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    String getSubInfo(
            @RequestParam(value = "phone") String phone) {
        LOG.info("getSubInfo::BEGIN");
        RestMessage resp = null;
        HashMap<String, Object> data = new HashMap<>();
        try {
            List<Subscriber> subscriber = subscriberService.findAllByMisdn(phone);
            data.put("subscriber", subscriber);
            List<VasPackage> listVasPackage = vasPackageService.findAll();
            data.put("listVasPackage", listVasPackage);
            resp = RestMessage.RestMessageBuilder.SUCCESS();
            resp.setData(data);
        } catch (Exception ex) {
            LOG.error("", ex);
        }
        LOG.info("getSubInfo::END");
        return GSON_ALL.toJson(resp);
    }

    @RequestMapping(value = "sub_info_login", method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    String getAllSubByMsisdnAndMpin(
            @RequestParam(value = "phone") String phone,
            @RequestParam(value = "mpin") String mpin) {
        LOG.info("getAllSubByMsisdnAndMpin::BEGIN");
        RestMessage resp = null;
        try {
            List<Subscriber> subscriber = subscriberService.findAllByMisdnAndMpin(phone, mpin);
            if (subscriber.size() != 0) {
                resp = RestMessage.RestMessageBuilder.SUCCESS();
            } else {
                resp = RestMessage.RestMessageBuilder.FAIL("1", "Fail");
            }
            resp.setData(subscriber);
        } catch (Exception ex) {
            LOG.error("", ex);
        }
        LOG.info("getAllSubByMsisdnAndMpin::END");
        return GSON_ALL.toJson(resp);
    }

    @RequestMapping(value = "sub_info_package_status", method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    String getAllSubByMsisdn(
            @RequestParam(value = "phone") String phone) {
        LOG.info("getAllSubByMsisdn::BEGIN");
        RestMessage resp = null;
        try {
            List<SubscriberModel> listSubs = new ArrayList<>();
            List<Subscriber> subscriber = subscriberService.findAllByMisdn(phone);
            if (subscriber.size() != 0) {
                for (Subscriber sub : subscriber) {
                    SubscriberModel subscriberModel = new SubscriberModel();
                    subscriberModel.setMsisdn(sub.getMsisdn());
                    subscriberModel.setPackageId(sub.getPackageId().toString());
                    subscriberModel.setStatus(sub.getStatus());
                    listSubs.add(subscriberModel);
                }
                resp = RestMessage.RestMessageBuilder.SUCCESS();
            } else {
                resp = RestMessage.RestMessageBuilder.FAIL("1", "Fail");
            }
            resp.setData(listSubs);
        } catch (Exception ex) {
            LOG.error("", ex);
        }
        LOG.info("getAllSubByMsisdn::END");
        return GSON_ALL.toJson(resp);
    }

//    @RequestMapping(value = "sub_charge_log", method = {RequestMethod.GET, RequestMethod.POST})
//    public @ResponseBody
//    String getSubChargeLog(
//            @RequestParam(value = "startdate") String startDate,
//            @RequestParam(value = "enddate") String endDate,
//            @RequestParam(value = "phone") String phone,
//            @RequestParam(value = "type") String type,
//            @RequestParam(value = "status") String status) {
//        LOG.info("getSubChargeLog::BEGIN");
//        RestMessage resp = null;
//        List<SubsChargeLog> subsChargeLogs = null;
//        try {
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
//            Date _fromDate = sdf.parse(startDate);
//            Date _toDate = sdf.parse(endDate);
//            //+1 Day cho enDate
//            Calendar c = Calendar.getInstance();
//            c.setTime(_toDate);
//            c.add(Calendar.DAY_OF_MONTH, 1);
//            _toDate = c.getTime();
//
//            int _status;
//            if (status.equals("")) {
//                _status = 2;
//            } else {
//                _status = Integer.parseInt(status);
//            }
//            if (type.equals("REG")) {//dang ki moi
//                subsChargeLogs = subsChargeLogService.findSubsChargeLogReg(_fromDate, _toDate, phone, _status);
//            } else if (type.equals("RE_REGISTER_SUB")) {//dang ky lai
//                subsChargeLogs = subsChargeLogService.findSubsChargeLogReRegister(_fromDate, _toDate, phone, _status);
//                for (SubsChargeLog sb : subsChargeLogs) {
//                    sb.setType("RE_REGISTER_SUB");
//                }
//            } else {//gia han, huy
//                subsChargeLogs = subsChargeLogService.findSubsChargeLog(_fromDate, _toDate, phone, type, _status);
//            }
//            resp = RestMessage.RestMessageBuilder.SUCCESS();
//            resp.setData(subsChargeLogs);
//        } catch (Exception ex) {
//            LOG.error("", ex);
//        }
//        LOG.info("getSubChargeLog::END");
//        return GSON_ALL.toJson(resp);
//    }

    @RequestMapping(value = "cdr_charge_log", method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    String getCdrChargeLog(
            @RequestParam(value = "startdate") String startDate,
            @RequestParam(value = "enddate") String endDate,
            @RequestParam(value = "phone") String phone,
            @RequestParam(value = "type") String type) {
        LOG.info("getCdrChargeLog::BEGIN");
        RestMessage resp = null;
        String _phone = "";
        Date now = new Date();
        CdrChargeHistoryModel cdrChargeModel = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            Date _fromDate = sdf.parse(startDate);
            Date _toDate = sdf.parse(endDate);
            //+1 ngày cho enDate
            Calendar c = Calendar.getInstance();
            c.setTime(_toDate);
            c.add(Calendar.DAY_OF_MONTH, 1);
            _toDate = c.getTime();

            if (!"".equals(phone) && phone != null) {
                _phone = "84" + phone;
            }
            List<CdrChargeHistoryModel> cdrChargeHistory = new ArrayList<>();
            List<CdrLog> lstCdrChargeLogs = cdrLogService.findCdrChargeAmount(_fromDate, _toDate, _phone, type);
            //lấy thông tin trừ cước không phải ngày hiện tại trong cdr_log
            if (lstCdrChargeLogs != null && !lstCdrChargeLogs.isEmpty()) {
                for (CdrLog cdr : lstCdrChargeLogs) {
                    cdrChargeModel = new CdrChargeHistoryModel();
                    cdrChargeModel.setMsisdn(cdr.getMsisdn());
                    cdrChargeModel.setAmount(cdr.getAmount());
                    cdrChargeModel.setTime1(cdr.getTime1());
                    if (cdr.getContentId() != null) {
                        cdrChargeModel.setPackageName(getPackageByContentId(cdr.getContentId(), cdr.getAmount()));
                    }
                    if (!"".equals(cdr.getType()) && cdr.getType() != null) {
                        cdrChargeModel.setType(cdr.getType());
                    } else {
                        cdrChargeModel.setType("RENEW_DAY");
                    }
                    cdrChargeModel.setStatus(1);
                    cdrChargeHistory.add(cdrChargeModel);
                }
            }
            //nếu enDate = ngày hiện tại thì lấy thông tin trừ cước trong bảng core_subs_charge_log
            String _now = sdf.format(now);
            if (_now.equals(endDate)) {
                List<Object[]> lstSubsChargeLog = subsChargeLogService.findSubsChargeLog(phone, type);
                if (lstSubsChargeLog != null && !lstSubsChargeLog.isEmpty()) {
                    for (Object[] sub : lstSubsChargeLog) {
                        cdrChargeModel = new CdrChargeHistoryModel();
                        cdrChargeModel.setMsisdn(String.valueOf("84" + sub[0]));
                        cdrChargeModel.setPackageName(String.valueOf(sub[1]));
                        cdrChargeModel.setTime1(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(sub[2].toString()));
                        cdrChargeModel.setAmount(Integer.valueOf(sub[3].toString()));
                        cdrChargeModel.setType(String.valueOf(sub[4]));
                        cdrChargeModel.setStatus(Integer.parseInt(sub[5].toString()));
                        cdrChargeHistory.add(cdrChargeModel);
                    }
                }
            }

            resp = RestMessage.RestMessageBuilder.SUCCESS();
            resp.setData(cdrChargeHistory);
        } catch (Exception ex) {
            LOG.error("", ex);
        }
        LOG.info("getCdrChargeLog::END");
        return GSON_ALL.toJson(resp);
    }

    @RequestMapping(value = "sub_info_findOne.html", method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    String getSubInfoFindOne(
            @RequestParam(value = "id") String id) {
        LOG.info("getSubInfo::BEGIN");
        RestMessage resp = null;
        HashMap<String, Object> data = new HashMap<>();
        try {
            Subscriber subscriber = subscriberService.findOne(Integer.parseInt(id));
            if (subscriber != null) {
                SubscriberModel subscriberModel = new SubscriberModel();
                subscriberModel.setId(subscriber.getId().toString());
                subscriberModel.setMsisdn(subscriber.getMsisdn());
                subscriberModel.setProductId(subscriber.getProductId().toString());
                subscriberModel.setPackageId(subscriber.getPackageId().toString());
                data.put("subscriber", subscriberModel);
            }
            List<VasPackage> listVasPackage = vasPackageService.findAll();
            List<VaspackageModel> lstVaspackageModel = new ArrayList<>();
            for (VasPackage vasPackage : listVasPackage) {
                VaspackageModel vaspackageModel = new VaspackageModel();
                vaspackageModel.setId(vasPackage.getId().toString());
                vaspackageModel.setProductId(vasPackage.getProductId().toString());
                vaspackageModel.setName(vasPackage.getName());
                lstVaspackageModel.add(vaspackageModel);
            }
            data.put("listVasPackage", lstVaspackageModel);
            resp = RestMessage.RestMessageBuilder.SUCCESS();
            resp.setData(data);
        } catch (Exception ex) {
            LOG.error("", ex);
        }
        LOG.info("getSubInfo::END");
        return GSON_ALL.toJson(resp);
    }

    private String getPackageByContentId(String contentId, int amount) {
        String packageName = "";
        try {
            if ("0000000003".equals(contentId)) {
                if (amount == 2000) {
                    packageName = "PT";
                } else if (amount == 10000) {
                    packageName = "PT7";
                } else if (amount == 1000) {
                    packageName = "KP";
                } else {
                    packageName = "KP7";
                }
            } else if ("0000000001".equals(contentId)) {
                packageName = "QT";
            } else {
                packageName = "CV";
            }
        } catch (Exception ex) {
            LOG.error("", ex);
        }
        return packageName;
    }
}
