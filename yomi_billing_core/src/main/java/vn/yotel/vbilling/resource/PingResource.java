package vn.yotel.vbilling.resource;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.yotel.vbilling.jpa.Subscriber;
import vn.yotel.vbilling.model.ResponseData;
import vn.yotel.vbilling.model.SubscriberModel;
import vn.yotel.vbilling.service.SubscriberService;

import java.util.ArrayList;
import java.util.List;

@Component
@Path(value = "/ping")
@Produces(value = { MediaType.APPLICATION_JSON })
@Consumes(value = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
public class PingResource {

	private static final Logger logger = LoggerFactory.getLogger(PingResource.class);

	@Autowired
	SubscriberService subscriberService;

	@Context
	private HttpServletRequest request;

	@Context
	private HttpHeaders httpHeaders;

	@GET
	public Response ping() {
		logger.info("Ready");
		ResponseData responseData = ResponseData.responseData("1", "Ready");
		return Response.ok(responseData).type(MediaType.APPLICATION_JSON_TYPE).build();
	}

	@POST
	@Path(value = "/checkLogin")
	public ResponseData checkLogin(@Context HttpServletRequest req,
								   @FormParam("msisdn") String msisdn,
								   @FormParam("mpin") String mpin) {
		logger.info("checkLogin");
		ResponseData responseData = new ResponseData();
		try {
			List<Subscriber> subscriber = subscriberService.findAllByMisdnAndMpin(msisdn, mpin);
			if (subscriber != null && !subscriber.isEmpty()) {
				SubscriberModel bean = new SubscriberModel();
				bean.setMsisdn(subscriber.get(0).getMsisdn());
				bean.setPackageId(String.valueOf(subscriber.get(0).getPackageId()));
				bean.setStatus(subscriber.get(0).getStatus());
				responseData.setData(bean);
				responseData.setResultCode("1");
				responseData.setResult("Ok");
			} else {
				return ResponseData.responseData("2", "Login error");
			}
		} catch (Exception ex) {
			logger.error("", ex);
			return ResponseData.responseData("9999", "Exception");
		}
		return responseData;
	}

	@POST
	@Path(value = "/checkPackageStatus")
	public ResponseData checkPackageStatus(@Context HttpServletRequest req,
								   @FormParam("msisdn") String msisdn) {
		logger.info("checkPackageStatus");
		ResponseData responseData = new ResponseData();
		try {
			List<SubscriberModel> listSubs = new ArrayList<>();
			List<Subscriber> subscriber = subscriberService.findAllByMisdn(msisdn);
			if (subscriber != null && !subscriber.isEmpty()) {
				for (Subscriber sub: subscriber) {
					SubscriberModel subscriberModel = new SubscriberModel();
					subscriberModel.setMsisdn(sub.getMsisdn());
					subscriberModel.setPackageId(sub.getPackageId().toString());
					subscriberModel.setStatus(sub.getStatus());
					listSubs.add(subscriberModel);
				}
				responseData.setData(listSubs);
				responseData.setResultCode("1");
				responseData.setResult("Ok");
			} else {
				return ResponseData.responseData("2", "Fail");
			}
		} catch (Exception ex) {
			logger.error("", ex);
			return ResponseData.responseData("9999", "Exception");
		}
		return responseData;
	}

}
