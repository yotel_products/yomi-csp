package vn.yotel.vbilling.thread;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.regex.Pattern;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vn.yotel.admin.jpa.SysParam;
import vn.yotel.admin.service.SysParamService;
import vn.yotel.commons.context.AppContext;
import vn.yotel.commons.exception.AppException;
import vn.yotel.commons.util.StringUtils;
import vn.yotel.commons.util.Util;
import vn.yotel.thread.ManageableThread;
import vn.yotel.vbilling.jpa.*;
import vn.yotel.vbilling.model.*;
import vn.yotel.vbilling.resource.ChargingResource;
import vn.yotel.vbilling.service.*;
import vn.yotel.vbilling.util.ChargingCSPClient;
import vn.yotel.vbilling.util.DateValidator;
import vn.yotel.vbilling.util.MessageBuilder;
import vn.yotel.vbilling.util.Utils;
import vn.yotel.yomi.AppParams;
import vn.yotel.yomi.Constants;

import javax.annotation.Resource;

public class ProcessMoRequest extends ManageableThread {

    private static Logger LOG = LoggerFactory.getLogger(ProcessMoRequest.class);
    private static final Gson GSON_ALL = new GsonBuilder().serializeNulls().create();
    private static final SimpleDateFormat sdf_DDMMYYYY = new SimpleDateFormat("dd/MM/yyyy");
    private static final SimpleDateFormat sdf_HHmm = new SimpleDateFormat("HH:mm");
    private static final SimpleDateFormat sdf_DDMMYYYYHHmmss = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    private ConcurrentLinkedQueue<MORequest> moQueue;
    private Object moQueueNotifier;
    private ConcurrentLinkedQueue<MTRequest> mtQueueToCSP;
    private Object mtQueueToCSPNotifier;
    private ConcurrentLinkedQueue<MORequest> moProcessQueue;
    private Object moProcessQueueNotifier;

    private List<SmsSyntax> allSyntaxs = null;
    private MtModel mtModel = new MtModel();
    private SmsService smsService;
    private SysParamService sysParamService;
    private XsPromotionService xsPromotionService;
    private XsPromotionLogService xsPromotionLogService;
    private ChargingCSPClient chargingCSPClient;
    private ChargeLogService chargeLogService;
    private SubsChargeLogService subsChargeLogService;

    private final String AS_PACKAGE_CODE_KEY = "AS_PACKAGE_CODE";
    private final String AS_TIME_START_KEY = "AS_TIME_START";
    private final String AS_TIME_END_KEY = "AS_TIME_END";
    private final String AS_NOT_TIME_PROMOTION_KEY = "AS_NOT_TIME_PROMOTION";
    private final String AS_NOT_ENOUGH_MONEY_KEY = "AS_NOT_ENOUGH_MONEY";
    private final String AS_MIN_NUMBER_KEY = "AS_MIN_NUMBER";
    private final String AS_MAX_NUMBER_KEY = "AS_MAX_NUMBER";
    private final String AS_NOTIFICATION_MESSAGE_KEY = "AS_NOTIFICATION_MESSAGE";

    private String AS_PACKAGE_CODE_VALUE = "";
    private String AS_TIME_START_VALUE = "";
    private String AS_TIME_END_VALUE = "";
    private Date dtCurrentTime = null;
    private int iMinNumber = 1;
    private int iMaxNumber = 50000;
    private String strPrivateNumber = "934538590";

    private SubscriberService subscriberService;
    private CpGateService cpGateService;
    private ContentLogService contentLogService;
    private CpCvptService cpCvptService;
    private BuySingleLogService buySingleLogService;

//	private HashMap<String, Boolean> data = new HashMap<>();

    @SuppressWarnings("unchecked")
    @Override
    protected void loadParameters() throws AppException {
        if (this.params != null) {
        } else {
            LOG.warn("Could not get parameters from the configuration file");
        }
        moQueue = (ConcurrentLinkedQueue<MORequest>) AppContext.getBean("moQueue");
        moQueueNotifier = AppContext.getBean("moQueueNotifier");

        mtQueueToCSP = (ConcurrentLinkedQueue<MTRequest>) AppContext.getBean("mtQueueToCSP");
        mtQueueToCSPNotifier = AppContext.getBean("mtQueueToCSPNotifier");

        moProcessQueue = (ConcurrentLinkedQueue<MORequest>) AppContext.getBean("moProcessQueue");
        moProcessQueueNotifier = AppContext.getBean("moProcessQueueNotifier");
        this.allSyntaxs = null;
    }

    @Override
    protected void initializeSession() throws AppException {
        smsService = (SmsService) AppContext.getBean("smsService");
        sysParamService = (SysParamService) AppContext.getBean("sysParamService");
        xsPromotionService = (XsPromotionService) AppContext.getBean("xsPromotionService");
        xsPromotionLogService = (XsPromotionLogService) AppContext.getBean("xsPromotionLogService");
        chargeLogService = (ChargeLogService) AppContext.getBean("chargeLogService");
        chargingCSPClient = (ChargingCSPClient) getBean("chargingCSPClient");

        subscriberService = (SubscriberService) AppContext.getBean("subscriberService");
        cpGateService = (CpGateService) AppContext.getBean("cpGateService");
        cpCvptService = (CpCvptService) AppContext.getBean("cpCvptService");
        buySingleLogService = (BuySingleLogService) AppContext.getBean("buySingleLogService");
        contentLogService = (ContentLogService) AppContext.getBean("contentLogService");
        subsChargeLogService = (SubsChargeLogService) AppContext.getBean("subsChargeLogService");

        SysParam sysParam = sysParamService.findByKey(AS_PACKAGE_CODE_KEY);
        if (sysParam != null) {
            AS_PACKAGE_CODE_VALUE = sysParam.getValue();
        }
        sysParam = sysParamService.findByKey(AS_TIME_START_KEY);
        if (sysParam != null) {
            AS_TIME_START_VALUE = sysParam.getValue();
        } else {
            AS_TIME_START_VALUE = "08:00:00";
        }
        sysParam = sysParamService.findByKey(AS_TIME_END_KEY);
        if (sysParam != null) {
            AS_TIME_END_VALUE = sysParam.getValue();
        } else {
            AS_TIME_END_VALUE = "21:59:59";
        }

        LOG.debug("AS_PACKAGE_CODE_VALUE: {}", AS_PACKAGE_CODE_VALUE);
        LOG.debug("AS_TIME_START_VALUE: {}", AS_TIME_START_VALUE);
        LOG.debug("AS_TIME_END_VALUE: {}", AS_TIME_END_VALUE);

        try {
            sysParam = sysParamService.findByKey(AS_MIN_NUMBER_KEY);
            if (sysParam != null) {
                iMinNumber = Integer.parseInt(sysParam.getValue());
            }
            sysParam = sysParamService.findByKey(AS_MAX_NUMBER_KEY);
            if (sysParam != null) {
                iMaxNumber = Integer.parseInt(sysParam.getValue());
            }
        } catch (Exception ex) {
            LOG.error("ERROR_PARSE_NUMBER:", ex);
        }

        mtModel = smsService.mtModel();
    }

    @Override
    protected boolean processSession() throws AppException {
        try {
            while (!requireStop) {
                MORequest moReq = moQueue.poll();
                if (moReq != null) {
                    LOG.info("Process MO request: {}", moReq.toString());
                    LOG.info("Processed : ", moReq.isProcessed());
                    if (!moReq.isProcessed()) {
                        MTRequest mtReq = null;
                        boolean isValid = true;
                        String fromNumber = moReq.getFromNumber();
                        String toNumber = moReq.getToNumber();
                        VasPackage vasPackage = this.preProcessMO(moReq);
                        isValid = this.validateShortCode(toNumber);

                        LOG.info("isValid " + (isValid ? "true" : "false"));
                        LOG.info(vasPackage != null ? "vasPackage:" + (vasPackage.getName()) : "vasPackage: IS NULL");

                        if (isValid) {
                            String message = moReq.getMessage().toUpperCase();
                            message = message.trim();
                            boolean processed = processMO(moReq);
                            LOG.debug("msisdn=" + fromNumber + ";moCommand=" + moReq.getCommand() + ";processed=" + processed);
                            if (!processed) {
                                if (Arrays.asList(AS_PACKAGE_CODE_VALUE.split(";")).contains(moReq.getCommand())) {
                                    mtReq = MessageBuilder.buildMTRequest(toNumber, fromNumber, buildKPASMessage(moReq), moReq, moReq.getCommand());
                                } else if (Constants.CommandCode.GUIDE.equals(moReq.getCommand())) {
                                    moReq.setCommand(Constants.CommandCode.GUIDE);
                                    mtReq = MessageBuilder.buildMTRequest(toNumber, fromNumber, mtModel.getMtHD(), moReq, moReq.getCommand());
                                } else if ("CHANGE_PASS".equals(moReq.getCommand())) {
                                    moReq.setCommand("CHANGE_PASS");
                                    mtReq = MessageBuilder.buildMTRequest(toNumber, fromNumber, buildFotGotMessage(moReq), moReq, moReq.getCommand());
                                } else if ("CHANGE_PASS_ACCEPT".equals(moReq.getCommand())) {
                                    moReq.setCommand("CHANGE_PASS_ACCEPT");
                                    mtReq = MessageBuilder.buildMTRequest(toNumber, fromNumber, buildFotGotMessage(moReq), moReq, moReq.getCommand());
                                } else if ("NS".equals(moReq.getCommand())) {
                                    String content = builCVPTMessage(moReq);
                                    if (!content.equalsIgnoreCase("")) {
                                        mtReq = MessageBuilder.buildMTRequest(toNumber, fromNumber, content, moReq, moReq.getCommand());
                                    } else {
                                        mtReq = null;
                                    }
                                } else if (Constants.CommandCode.RESET_PWD.equals(moReq.getCommand())) {
                                    mtReq = MessageBuilder.buildMTRequest(toNumber, fromNumber, builChangePassMessage(moReq), moReq, moReq.getCommand());
                                } else if ("MUA".equals(moReq.getCommand())) {
                                    String mgs = builLessonMessage(moReq);
                                    if (!mgs.equalsIgnoreCase("")) {
                                        mtReq = MessageBuilder.buildMTRequest(toNumber, fromNumber, mgs, moReq, moReq.getCommand());
                                    } else {
                                        mtReq = null;
                                    }
                                } else {
                                    mtReq = processDefaultMoRequest(moReq);
                                    LOG.info("::" + mtReq.getMessage() != null ? mtReq.getMessage() : "IS NULL MESSAGE");
                                }
                            }
                        }
                        if (mtReq != null) {
                            mtQueueToCSP.offer(mtReq);
                            synchronized (mtQueueToCSPNotifier) {
                                mtQueueToCSPNotifier.notifyAll();
                            }
                        }
                    }
                    this.logMoRequest(moReq);
                } else {
                    synchronized (moQueueNotifier) {
                        moQueueNotifier.wait(100L);
                    }
                }
            }
        } catch (Exception e) {
            LOG.error("", e);
        }
        return true;
    }

    /**
     * Should override in each application
     *
     * @param moReq
     * @return
     */
    protected boolean processMO(MORequest moReq) {
        List<String> processList = getProcessList();
        if (processList.contains(moReq.getCommand())) {
            moProcessQueue.offer(moReq);
            synchronized (moProcessQueueNotifier) {
                moProcessQueueNotifier.notifyAll();
            }
            return true;
        } else {
            return false;
        }
    }

    protected MTRequest processDefaultMoRequest(MORequest moReq) {
        moReq.setCommand(Constants.CommandCode.DEFAULT);
        MTRequest mtReq = MessageBuilder.buildMTRequest(moReq.getToNumber(), moReq.getFromNumber(),
                mtModel.getMtInvalid(), moReq, moReq.getCommand());
        LOG.info("MTRequest::" + mtReq.getMessage());
        return mtReq;
    }

    protected List<String> getProcessList() {
        List<String> processList = Arrays.asList(Constants.CommandCode.SET_PWD, Constants.CommandCode.BUY,
                Constants.CommandCode.CHECK, Constants.CommandCode.GUIDE_DT, Constants.CommandCode.GUIDE_XS);
        return processList;
    }

    private void logMoRequest(MORequest moReq) {
        MoSms record = new MoSms();
        record.setShortCode(moReq.getToNumber());
        record.setMsisdn(moReq.getFromNumber());
        record.setMessage(moReq.getMessage().toUpperCase());
        record.setSmscId(moReq.getSmsId());
        record.setServiceCode(AppParams.PRODUCT_NAME);
        record.setKeyword(moReq.getCommand());
        record.setCreatedDate(moReq.getReceivedDate());
        smsService.create(record);
    }

    private boolean validateShortCode(String toNumber) {
        String regex = "\\d{0,9}" + AppParams.SHORT_CODE + "$";
        return toNumber.matches(regex);
    }

    /**
     * Base on shortcode & shortmessage to find out service and package
     * subscriber wants to use
     *
     * @param moReq
     */
    protected VasPackage preProcessMO(MORequest moReq) {
        String message = moReq.getMessage().toUpperCase();
        message = message.trim();
        VasPackage result = null;
        for (SmsSyntax packageSmsSyntax : getAllSyntaxs()) {
            if (message.matches(packageSmsSyntax.getRegex())) {
                LOG.info("{} is matched: {}", message, packageSmsSyntax.getRegex());
                result = packageSmsSyntax.getVasPackage();
                moReq.setCommand(packageSmsSyntax.getCommand());
                moReq.setSubsPackage(result);
                break;
            } else {
                // LOG.debug("{} is not matched: {}", message,
                // packageSmsSyntax.getRegex());
            }
        }
        return result;
    }

    @Override
    protected void completeSession() throws AppException {
    }

    protected List<SmsSyntax> getAllSyntaxs() {
        if (this.allSyntaxs == null || allSyntaxs.isEmpty()) {
            allSyntaxs = smsService.findAllSmsSyntax();
            for (SmsSyntax packageSmsSyntax : allSyntaxs) {
                // Load because of lazy load setting
                packageSmsSyntax.getVasPackage().getId();
            }
        }
        return this.allSyntaxs;
    }

    protected String buildKPASMessage(MORequest moReq) {
//		Calendar cal = Calendar.getInstance();
//		cal.set(Calendar.YEAR, 2019);
//		cal.set(Calendar.MONTH, 2);
//		cal.set(Calendar.DATE, 12);
//		cal.set(Calendar.HOUR, 23);
//		cal.set(Calendar.MINUTE, 59);
//		cal.set(Calendar.SECOND, 59);
//
//		Calendar calCurrent = Calendar.getInstance();
//
//		//Neu thoi gian hien tai lon hon thoi gian hieu luc CTKM thi gui MT sai cu phap
//		if (calCurrent.getTime().after(cal.getTime())) {
//			return mtModel.getMtInvalid();
//		}

        dtCurrentTime = Calendar.getInstance().getTime();
        StringBuilder sb = new StringBuilder();
        SmsModel smsModel = new SmsModel();
        Date dtStartTime = null;
        Date dtEndTime = null;
        try {
            String strCurrentTime = sdf_DDMMYYYY.format(dtCurrentTime);
            dtStartTime = sdf_DDMMYYYYHHmmss.parse(strCurrentTime + " " + AS_TIME_START_VALUE);
            dtEndTime = sdf_DDMMYYYYHHmmss.parse(strCurrentTime + " " + AS_TIME_END_VALUE);
            SysParam sysParamSMS = sysParamService.findByKey(moReq.getCommand());
            if (sysParamSMS != null) {
                smsModel = GSON_ALL.fromJson(sysParamSMS.getValue(), SmsModel.class);
                List<XsPromotion> lstPromotion = xsPromotionService.findPromotion(moReq.getFromNumber(), "ACTIVE");

                if ("AS_HD".equals(moReq.getCommand())) {
                    sb.append(smsModel.getMtContent1());
                } else if ("AS_CHECK_MT".equals(moReq.getCommand())) {
                    if (lstPromotion != null && lstPromotion.size() > 0) {
                        sb.append(smsModel.getMtContent1().replace("<SO_TIN_MP>", String.valueOf(lstPromotion.get(0).getNumberSms())));
                    } else {
                        sb.append(smsModel.getMtContent2());
                    }
                } else if ("AS_MS_POINT".equals(moReq.getCommand())) {
                    if (lstPromotion != null && lstPromotion.size() > 0) {
                        sb.append(smsModel.getMtContent1().replaceAll("<NGAY>", strCurrentTime));
                    } else {
                        sb.append(smsModel.getMtContent2());
                    }
                } else if ("AS_CHECK_POINT".equals(moReq.getCommand())) {
                    if (lstPromotion != null && lstPromotion.size() > 0) {
                        sb.append(smsModel.getMtContent1().replace("<TONG_DIEM_TB>", String.valueOf(lstPromotion.get(0).getNumber())));
                    } else {
                        sb.append(smsModel.getMtContent2());
                    }
                } else if ("AS_PICK_NUMBER_1".equals(moReq.getCommand())) {
                    if (dtCurrentTime.getTime() >= dtStartTime.getTime() && dtCurrentTime.getTime() <= dtEndTime.getTime()) {
                        if (lstPromotion != null && lstPromotion.size() > 0) {
                            XsPromotion xsPromotion = lstPromotion.get(0);
                            try {
                                String strNumPick = moReq.getMessage().toUpperCase().replaceFirst("AS\\s+", "");
//                            	LOG.debug("NUMBER_AS==" + strNumPick + "|");
                                int iNumPick = Integer.valueOf(strNumPick).intValue();

                                if (iNumPick < iMinNumber || iNumPick > iMaxNumber) {
                                    sysParamSMS = sysParamService.findByKey("AS_MIN_MAX_MESSAGE");
                                    if (sysParamSMS != null) {
                                        smsModel = GSON_ALL.fromJson(sysParamSMS.getValue(), SmsModel.class);
                                        if (xsPromotion.getNumberSms() > 0) {
                                            sb.append(smsModel.getMtContent1());
                                        } else {
                                            sb.append(smsModel.getMtContent2());
                                        }
                                    }
                                } else if (xsPromotion.getArrNumberPick() != null && !"".equals(xsPromotion.getArrNumberPick())
                                        && Arrays.asList(xsPromotion.getArrNumberPick().split(";")).contains(String.valueOf(iNumPick))) {
                                    sb.append(smsModel.getMtContent3().replace("<SO_NGUYEN_DUONG>", String.valueOf(iNumPick)));

                                    xsPromotion.setModifyTime(new Timestamp(dtCurrentTime.getTime()));

                                    //Tru di so luot dat mien phi
                                    xsPromotion.setNumberSms(xsPromotion.getNumberSms() - 1);
                                    xsPromotionService.update(xsPromotion);

                                    //Luu log pick number
//									XsPromotionLog xsPromotionLog = new XsPromotionLog();
//									xsPromotionLog.setCreatedDate(dtCurrentTime);
//									xsPromotionLog.setCreatedTime(new Timestamp(dtCurrentTime.getTime()));
//									xsPromotionLog.setMsisdn(xsPromotion.getMsisdn());
//									xsPromotionLog.setNumberPick(String.valueOf(iNumPick));
//									xsPromotionLog.setStatus(1);
//									xsPromotionLog.setTimeId(xsPromotion.getTimeId());
//									xsPromotionLogService.create(xsPromotionLog);
                                } else {
                                    if (xsPromotion.getNumberSms() > 1) {
                                        sb.append(smsModel.getMtContent1().replace("<SO_NGUYEN_DUONG>", String.valueOf(iNumPick)));
                                        if (xsPromotion.getArrNumberPick() == null || "".equals(xsPromotion.getArrNumberPick())) {
                                            xsPromotion.setArrNumberPick(";" + iNumPick);
                                        } else {
                                            xsPromotion.setArrNumberPick(xsPromotion.getArrNumberPick() + ";" + iNumPick);
                                        }

                                        xsPromotion.setModifyTime(new Timestamp(dtCurrentTime.getTime()));

                                        //Tru di so luot dat mien phi
                                        xsPromotion.setNumberSms(xsPromotion.getNumberSms() - 1);
                                        xsPromotionService.update(xsPromotion);

//										List<XsPromotionLog> lstPromotionLog = xsPromotionLogService.findByDate(dtCurrentTime);
                                        List<XsPromotionLog> lstPromotionLog_Old = xsPromotionLogService.findNumberMaxByDate(dtCurrentTime);
                                        LOG.info("lstPromotionLog_Old.1: " + GSON_ALL.toJson(lstPromotionLog_Old));

                                        //Luu log pick number
                                        XsPromotionLog xsPromotionLog = new XsPromotionLog();
                                        xsPromotionLog.setCreatedDate(dtCurrentTime);
                                        xsPromotionLog.setCreatedTime(new Timestamp(dtCurrentTime.getTime()));
                                        xsPromotionLog.setMsisdn(xsPromotion.getMsisdn());
                                        xsPromotionLog.setNumberPick(String.valueOf(iNumPick));
                                        xsPromotionLog.setStatus(1);
                                        xsPromotionLog.setTimeId(xsPromotion.getTimeId());
                                        xsPromotionLogService.create(xsPromotionLog);

                                        checkNumber(lstPromotionLog_Old, xsPromotionLog);

                                    } else if (xsPromotion.getNumberSms() == 1) {
                                        sb.append(smsModel.getMtContent2().replace("<SO_NGUYEN_DUONG>", String.valueOf(iNumPick)));
                                        if (xsPromotion.getArrNumberPick() == null || "".equals(xsPromotion.getArrNumberPick())) {
                                            xsPromotion.setArrNumberPick(";" + iNumPick);
                                        } else {
                                            xsPromotion.setArrNumberPick(xsPromotion.getArrNumberPick() + ";" + iNumPick);
                                        }

                                        xsPromotion.setModifyTime(new Timestamp(dtCurrentTime.getTime()));

                                        //Tru di so luot dat mien phi
                                        xsPromotion.setNumberSms(xsPromotion.getNumberSms() - 1);
                                        xsPromotionService.update(xsPromotion);

//										List<XsPromotionLog> lstPromotionLog = xsPromotionLogService.findByDate(dtCurrentTime);
                                        List<XsPromotionLog> lstPromotionLog_Old = xsPromotionLogService.findNumberMaxByDate(dtCurrentTime);
                                        LOG.info("lstPromotionLog_Old.1: " + GSON_ALL.toJson(lstPromotionLog_Old));

                                        //Luu log pick number
                                        XsPromotionLog xsPromotionLog = new XsPromotionLog();
                                        xsPromotionLog.setCreatedDate(dtCurrentTime);
                                        xsPromotionLog.setCreatedTime(new Timestamp(dtCurrentTime.getTime()));
                                        xsPromotionLog.setMsisdn(xsPromotion.getMsisdn());
                                        xsPromotionLog.setNumberPick(String.valueOf(iNumPick));
                                        xsPromotionLog.setStatus(1);
                                        xsPromotionLog.setTimeId(xsPromotion.getTimeId());
                                        xsPromotionLogService.create(xsPromotionLog);

                                        checkNumber(lstPromotionLog_Old, xsPromotionLog);
                                    } else {
                                        //Goi len CCSP tru tien khi thue bao het luot mien phi
                                        String result = buyPackage(moReq);
                                        if ("1".equals(result) || "OK".equals(result)) {
                                            sb.append(smsModel.getMtContent2().replace("<SO_NGUYEN_DUONG>", String.valueOf(iNumPick)));
                                            if (xsPromotion.getArrNumberPick() == null || "".equals(xsPromotion.getArrNumberPick())) {
                                                xsPromotion.setArrNumberPick(";" + iNumPick);
                                            } else {
                                                xsPromotion.setArrNumberPick(xsPromotion.getArrNumberPick() + ";" + iNumPick);
                                            }

                                            xsPromotion.setModifyTime(new Timestamp(dtCurrentTime.getTime()));

                                            xsPromotionService.update(xsPromotion);

//											List<XsPromotionLog> lstPromotionLog = xsPromotionLogService.findByDate(dtCurrentTime);
                                            List<XsPromotionLog> lstPromotionLog_Old = xsPromotionLogService.findNumberMaxByDate(dtCurrentTime);
                                            LOG.info("lstPromotionLog_Old.1: " + GSON_ALL.toJson(lstPromotionLog_Old));

                                            XsPromotionLog xsPromotionLog = new XsPromotionLog();
                                            xsPromotionLog.setCreatedDate(dtCurrentTime);
                                            xsPromotionLog.setCreatedTime(new Timestamp(dtCurrentTime.getTime()));
                                            xsPromotionLog.setMsisdn(xsPromotion.getMsisdn());
                                            xsPromotionLog.setNumberPick(String.valueOf(iNumPick));
                                            xsPromotionLog.setStatus(1);
                                            xsPromotionLog.setTimeId(xsPromotion.getTimeId());
                                            xsPromotionLog.setPrice(1000);
                                            xsPromotionLogService.create(xsPromotionLog);

                                            checkNumber(lstPromotionLog_Old, xsPromotionLog);
                                        } else {
                                            sysParamSMS = sysParamService.findByKey(AS_NOT_ENOUGH_MONEY_KEY);
                                            if (sysParamSMS != null) {
                                                smsModel = GSON_ALL.fromJson(sysParamSMS.getValue(), SmsModel.class);
                                                sb.append(smsModel.getMtContent1());
                                            }
                                        }
                                    }
                                }
                            } catch (Exception ex1) {
                                LOG.error("ERROR_PARSE_NUMBER", ex1);
                                sb.append(smsModel.getMtContent5());
                            }
                        } else {
                            sb.append(smsModel.getMtContent4());
                        }
                    } else {
                        sysParamSMS = sysParamService.findByKey(AS_NOT_TIME_PROMOTION_KEY);
                        if (sysParamSMS != null) {
                            smsModel = GSON_ALL.fromJson(sysParamSMS.getValue(), SmsModel.class);
                            sb.append(smsModel.getMtContent1().replaceAll("<NGAY>", strCurrentTime));
                        }
                    }
                } else if ("AS_PICK_NUMBER_2".equals(moReq.getCommand())) {
                    if (dtCurrentTime.getTime() >= dtStartTime.getTime() && dtCurrentTime.getTime() <= dtEndTime.getTime()) {
                        if (lstPromotion != null && lstPromotion.size() > 0) {
                            XsPromotion xsPromotion = lstPromotion.get(0);
                            try {
                                String strNumPick = moReq.getMessage().toUpperCase().replaceFirst("DIEM\\s+", "");
//								LOG.debug("NUMBER_AS2==" + strNumPick + "|");
                                int iNumPick = Integer.valueOf(strNumPick).intValue();
                                if (iNumPick < iMinNumber || iNumPick > iMaxNumber) {
                                    sysParamSMS = sysParamService.findByKey("AS_MIN_MAX_MESSAGE");
                                    if (sysParamSMS != null) {
                                        smsModel = GSON_ALL.fromJson(sysParamSMS.getValue(), SmsModel.class);
                                        if (xsPromotion.getNumberSms() > 0) {
                                            sb.append(smsModel.getMtContent1());
                                        } else {
                                            sb.append(smsModel.getMtContent2());
                                        }
                                    }
                                } else if (xsPromotion.getArrNumberPick() != null && !"".equals(xsPromotion.getArrNumberPick())
                                        && Arrays.asList(xsPromotion.getArrNumberPick().split(";")).contains(String.valueOf(iNumPick))) {
                                    if (xsPromotion.getNumber() >= 1000) {
                                        sb.append(smsModel.getMtContent3().replace("<SO_NGUYEN_DUONG>", String.valueOf(iNumPick)));

                                        xsPromotion.setNumber(xsPromotion.getNumber() - 1000);
                                        xsPromotion.setModifyTime(new Timestamp(dtCurrentTime.getTime()));
                                        xsPromotionService.update(xsPromotion);

//										List<XsPromotionLog> lstPromotionLog = xsPromotionLogService.findByDate(dtCurrentTime);
                                        List<XsPromotionLog> lstPromotionLog_Old = xsPromotionLogService.findNumberMaxByDate(dtCurrentTime);
                                        LOG.info("lstPromotionLog_Old.1: " + GSON_ALL.toJson(lstPromotionLog_Old));

                                        XsPromotionLog xsPromotionLog = new XsPromotionLog();
                                        xsPromotionLog.setCreatedDate(dtCurrentTime);
                                        xsPromotionLog.setCreatedTime(new Timestamp(dtCurrentTime.getTime()));
                                        xsPromotionLog.setMsisdn(xsPromotion.getMsisdn());
                                        xsPromotionLog.setNumberPick(String.valueOf(iNumPick));
                                        xsPromotionLog.setStatus(1);
                                        xsPromotionLog.setTimeId(xsPromotion.getTimeId());
                                        xsPromotionLogService.create(xsPromotionLog);

                                        checkNumber(lstPromotionLog_Old, xsPromotionLog);
                                    } else {
                                        sb.append(smsModel.getMtContent2());
                                    }
                                } else {
                                    if (xsPromotion.getNumber() >= 1000) {
                                        xsPromotion.setNumber(xsPromotion.getNumber() - 1000);
                                        sb.append(smsModel.getMtContent1().replace("<TONG_DIEM_TB>", String.valueOf(xsPromotion.getNumber())));

                                        if (xsPromotion.getArrNumberPick() == null || "".equals(xsPromotion.getArrNumberPick())) {
                                            xsPromotion.setArrNumberPick(";" + iNumPick);
                                        } else {
                                            xsPromotion.setArrNumberPick(xsPromotion.getArrNumberPick() + ";" + iNumPick);
                                        }

                                        xsPromotion.setModifyTime(new Timestamp(dtCurrentTime.getTime()));
                                        xsPromotionService.update(xsPromotion);

//										List<XsPromotionLog> lstPromotionLog = xsPromotionLogService.findByDate(dtCurrentTime);
                                        List<XsPromotionLog> lstPromotionLog_Old = xsPromotionLogService.findNumberMaxByDate(dtCurrentTime);
                                        LOG.info("lstPromotionLog_Old.1: " + GSON_ALL.toJson(lstPromotionLog_Old));

                                        XsPromotionLog xsPromotionLog = new XsPromotionLog();
                                        xsPromotionLog.setCreatedDate(dtCurrentTime);
                                        xsPromotionLog.setCreatedTime(new Timestamp(dtCurrentTime.getTime()));
                                        xsPromotionLog.setMsisdn(xsPromotion.getMsisdn());
                                        xsPromotionLog.setNumberPick(String.valueOf(iNumPick));
                                        xsPromotionLog.setStatus(1);
                                        xsPromotionLog.setTimeId(xsPromotion.getTimeId());
                                        xsPromotionLogService.create(xsPromotionLog);

                                        checkNumber(lstPromotionLog_Old, xsPromotionLog);
                                    } else {
                                        sb.append(smsModel.getMtContent2());
                                    }
                                }
                            } catch (Exception ex1) {
                                LOG.error("ERROR_PARSE_NUMBER", ex1);
                                sb.append(smsModel.getMtContent5());
                            }
                        } else {
                            sb.append(smsModel.getMtContent4());
                        }
                    } else {
                        sysParamSMS = sysParamService.findByKey(AS_NOT_TIME_PROMOTION_KEY);
                        if (sysParamSMS != null) {
                            smsModel = GSON_ALL.fromJson(sysParamSMS.getValue(), SmsModel.class);
                            sb.append(smsModel.getMtContent1().replaceAll("<NGAY>", strCurrentTime));
                        }
                    }
                }
            }
        } catch (Exception ex) {
            LOG.error("ERROR_buildKPASMessage", ex);
            //GUI MT thong bao he thong loi
            MTRequest mtReqPrivate = MessageBuilder.buildMTRequest(AppParams.SHORT_CODE, strPrivateNumber,
                    "ERR_MESSAGE_KPAS " + ex.getMessage(), null, "ERR_MESSAGE_KPAS");
            if (mtReqPrivate != null) {
                mtQueueToCSP.offer(mtReqPrivate);
                synchronized (mtQueueToCSPNotifier) {
                    mtQueueToCSPNotifier.notifyAll();
                }
            }
        }
        return sb.toString();
    }

    private String buyPackage(MORequest moReq) {
        LOG.info("processBuyPackage: BEGIN -----");
        String result = "0";
        ChargeLog chargeLog = new ChargeLog();
        SubsChargeLog subsChargeLog = new SubsChargeLog();
        Date now = new Date();
        try {
            String content_ID = "0000000001";
            String category_ID = "000001";
            String spId = "001";
            String cpId = "001";
            result = chargingCSPClient.minusMoneyCheckMO(AppParams.SHORT_CODE,
                    moReq.getFromNumber(), moReq.getSmsId(),
                    "QT", "QT",
                    spId, cpId, content_ID, category_ID, "1000");
            LOG.error("processBuyPackage.result=", result);
            //Ghi log charge tai le cua KH
            chargeLog.setAmount(1000);
            chargeLog.setMsisdn(moReq.getFromNumber());
            chargeLog.setResultStatus("1".equals(result) || "OK".equals(result) ? true : false);
            chargeLog.setType("AS_BUY");
            chargeLogService.create(chargeLog);
            //Ghi log subs charge mua le cua KH
            subsChargeLog.setPriceCharge(1000);
            subsChargeLog.setMsisdn(moReq.getFromNumber());
            subsChargeLog.setStatus("1".equals(result) || "OK".equals(result) ? 1 : 0);
            subsChargeLog.setType("MUA");
            subsChargeLog.setOrgRequest(moReq.getMessage());
            subsChargeLog.setChargeDate(now);
            subsChargeLog.setPackageName("QT");
            subsChargeLogService.create(subsChargeLog);
        } catch (Exception ex) {
            LOG.error("processBuyPackage: ERROR", ex);
            //GUI MT thong bao he thong loi
            MTRequest mtReqPrivate = MessageBuilder.buildMTRequest(AppParams.SHORT_CODE, strPrivateNumber,
                    "ERR_BUY_PACKAGE " + ex.getMessage(), null, "ERR_BUY_PACKAGE");
            if (mtReqPrivate != null) {
                mtQueueToCSP.offer(mtReqPrivate);
                synchronized (mtQueueToCSPNotifier) {
                    mtQueueToCSPNotifier.notifyAll();
                }
            }
        }
        LOG.info("processBuyPackage: END -----");
        return result;
    }

    /**
     * Thuc hien so sanh LOG cu va LOG moi
     *
     * @param lstPromotionLogOld
     * @param newPromotionLog
     */
    private void checkNumber(List<XsPromotionLog> lstPromotionLogOld, XsPromotionLog newPromotionLog) {
        try {
            LOG.info("checkNumber::BEGIN--------------------------------------------------------");
            LOG.info("checkNumber|newPromotionLog=" + GSON_ALL.toJson(newPromotionLog));
            SysParam sysParam = sysParamService.findByKey(AS_NOTIFICATION_MESSAGE_KEY);
            SmsModel smsModel = new SmsModel();
            MTRequest mtReq = null;


            List<XsPromotionLog> lstNumberMax_LogOld = lstPromotionLogOld;
            LOG.info("lstNumberMax_LogOld: " + GSON_ALL.toJson(lstNumberMax_LogOld));
            //Neu LOG MAX khi bo log moi nhat vua dat
            if (!lstNumberMax_LogOld.isEmpty()) {
                XsPromotionLog xsNumberMaxOld = lstNumberMax_LogOld.get(0);
                //Gui MT khach hang bi mat vi tri cao nhat
                //Neu so cao nhat o LOG cu giong voi so moi dat thi gui tin mat uu the
                if (Integer.valueOf(xsNumberMaxOld.getNumberPick()).intValue() <= Integer.valueOf(newPromotionLog.getNumberPick()).intValue()) {
                    if (sysParam != null) {
                        LOG.info("checkNumber|SEND MT MISS MAX");
                        smsModel = GSON_ALL.fromJson(sysParam.getValue(), SmsModel.class);
                        String msg = smsModel.getMtContent3()
                                .replaceAll("<SO_DA_DAT>", xsNumberMaxOld.getNumberPick());

                        mtReq = MessageBuilder.buildMTRequest(AppParams.SHORT_CODE, xsNumberMaxOld.getMsisdn(), msg, null, "AS_SMS_NUMBER_OUT");
                        LOG.info("SUBSCRIBER {}:MISS MAX {}", xsNumberMaxOld.getMsisdn(), xsNumberMaxOld.getNumberPick());

                        if (mtReq != null) {
                            mtQueueToCSP.offer(mtReq);
                            synchronized (mtQueueToCSPNotifier) {
                                mtQueueToCSPNotifier.notifyAll();
                            }
                        }
                    }
                }
            }

            //Lay ra so thue bao dang duoc CAO NHAT va DUY NHAT
            List<XsPromotionLog> lstNumberMaxNew = xsPromotionLogService.findNumberMaxByDate(dtCurrentTime);
            ;
            LOG.info("lstNumberMaxNew: " + GSON_ALL.toJson(lstNumberMaxNew));
            if (!lstNumberMaxNew.isEmpty()) {
                XsPromotionLog xsNumberMax = lstNumberMaxNew.get(0);
                LOG.info("xsNumberMax" + GSON_ALL.toJson(xsNumberMax));
                //Neu ma so cao nhat LOG moi = so cao nhat thue bao vua dat thi gui tin ban la ng dat so cao nhat
                if (xsNumberMax.getMsisdn().equals(newPromotionLog.getMsisdn()) && xsNumberMax.getNumberPick().equals(newPromotionLog.getNumberPick())) {
                    //Gui MT khach hang vua dat vi tri cao nhat
                    if (sysParam != null) {
                        LOG.info("checkNumber|SEND MT MAX");
                        smsModel = GSON_ALL.fromJson(sysParam.getValue(), SmsModel.class);
                        String strDate = sdf_DDMMYYYY.format(newPromotionLog.getCreatedTime());
                        String strTime = sdf_HHmm.format(newPromotionLog.getCreatedTime());
                        String msg = smsModel.getMtContent4()
                                .replaceAll("<SO_DA_DAT>", newPromotionLog.getNumberPick())
                                .replaceAll("<NGAY>", strDate)
                                .replaceAll("<GIO_DAT>", strTime);

                        mtReq = MessageBuilder.buildMTRequest(AppParams.SHORT_CODE, newPromotionLog.getMsisdn(), msg, null, "AS_SMS_NUMBER_IN");
                        LOG.info("SUBSCRIBER {}:MAX {}", newPromotionLog.getMsisdn(), newPromotionLog.getNumberPick());

                        if (mtReq != null) {
                            mtQueueToCSP.offer(mtReq);
                            synchronized (mtQueueToCSPNotifier) {
                                mtQueueToCSPNotifier.notifyAll();
                            }
                        }
                    }
                }
            }

        } catch (Exception ex) {
            LOG.error("", ex);

            //GUI MT thong bao he thong loi
            MTRequest mtReqPrivate = MessageBuilder.buildMTRequest(AppParams.SHORT_CODE, strPrivateNumber,
                    "ERR_CHECK_MAX " + ex.getMessage(), null, "ERR_CHECK_MAX");
            if (mtReqPrivate != null) {
                mtQueueToCSP.offer(mtReqPrivate);
                synchronized (mtQueueToCSPNotifier) {
                    mtQueueToCSPNotifier.notifyAll();
                }
            }

        }
        LOG.info("checkNumber::END--------------------------------------------------------");
    }

    protected String buildFotGotMessage(MORequest moReq) {
        dtCurrentTime = Calendar.getInstance().getTime();
        StringBuilder sb = new StringBuilder();
        MtModel mtModel = new MtModel();
        String mpin = Util.generateMPIN();
        try {
            String strCurrentTime = sdf_DDMMYYYY.format(dtCurrentTime);
            SysParam sysParamSMS = sysParamService.findByKey("_MT_KEY");
            List<Subscriber> listSub = subscriberService.findActiveByMsisdn(moReq.getFromNumber());
            if (sysParamSMS != null) {
                mtModel = GSON_ALL.fromJson(sysParamSMS.getValue(), MtModel.class);
                if ("CHANGE_PASS_ACCEPT".equals(moReq.getCommand())) {
                    String message = mtModel.getMtMK().replaceFirst("<MATKHAU>", mpin);
                    if (listSub != null) {
                        for (Subscriber list : listSub) {
                            //thay doi ma pin
                            list.setMpin(mpin);
                            subscriberService.update(list);
                        }
                        //Update ma pin len webapp
                        HandlingResult postResult = cpGateService.notifyResetPassword(moReq.getSmsId(), moReq.getFromNumber(), mpin, strCurrentTime, message);

                        //GUI MT thong bao he thong mat dong bo MAT KHAU thue bao
                        if (postResult.parseToHttpCode() != 200) {
                            MTRequest mtReqPrivate = MessageBuilder.buildMTRequest(AppParams.SHORT_CODE, strPrivateNumber,
                                    "ERR_SYNC_MPIN_SUBS " + moReq.getFromNumber(), null, "ERR_SYNC_MPIN_SUBS");
                            if (mtReqPrivate != null) {
                                mtQueueToCSP.offer(mtReqPrivate);
                                synchronized (mtQueueToCSPNotifier) {
                                    mtQueueToCSPNotifier.notifyAll();
                                }
                            }
                        }
                        sb.append(message);
                    }
//					}
                } else if ("CHANGE_PASS".equals(moReq.getCommand())) {
                    sb.append(mtModel.getMtSmsFogotMessage().replaceFirst("<ISDN_FOGOT>", moReq.getFromNumber()));
                }
            }
        } catch (Exception ex) {
            LOG.error("ERROR_buildKPASMessage", ex);
        }
        return sb.toString();
    }

    private String builCVPTMessage(MORequest moReq) {
        LOG.info("BEGIN::builCVPTMessage");
        String content = "";
        MTRequest mtReq = null;
        try {
            String mess = moReq.getMessage().toUpperCase();
            String dob = mess.replace("NS", "");

            //DateTime dtCurrent = new DateTime();
            //Date _dtEndOfDay = Utils.getEndOfDay(dtCurrent);
            //String expricedDate = new SimpleDateFormat("yyyyMMddHHmmss").format(_dtEndOfDay);

            //check dob co dung dinh dang khong
            DateValidator dateValidator = new DateValidator();
            boolean checkDob = dateValidator.validate(dob.trim());
            if (checkDob) {
                //gui mt ghi nhan ngay sinh dung dinh dang
                mtReq = MessageBuilder.buildMTRequest(moReq.getToNumber(), moReq.getFromNumber(), smsService.mtModelKeyCV().getMtNSSucces(), moReq, moReq.getCommand());
                if (mtReq != null) {
                    mtQueueToCSP.offer(mtReq);
                    synchronized (mtQueueToCSPNotifier) {
                        mtQueueToCSPNotifier.notifyAll();
                    }
                }
                Date _dob = Utils.parseDate(dob, moReq.getFromNumber());
                String _dobnew = new SimpleDateFormat("yyyyMMdd").format(_dob);
                //goi Api update
                //status NS_UPDATE update ngay sinh
                HandlingResult postResult = cpCvptService.notifyPTApi(moReq.getSmsId(), moReq.getFromNumber(), _dobnew, "", "NS_UPDATE", "");
                LOG.info("RESPONSE_CVPT_NS_UPDATE::" + postResult.parseResp());
//                CVPTModelUpdate ptModel = null;
//                if (postResult.getStatus().equalsIgnoreCase("0")) {
//                     ptModel = GSON_ALL.fromJson(postResult.parseResp(), CVPTModelUpdate.class);
//                }
                //update ngay sinh vao bang content_log theo sdt
                ContentLog contentLog = contentLogService.findByMsisdn(moReq.getFromNumber());
                if (contentLog != null) {
                    contentLog.setBirthday(dob);
                    contentLog.setOrgMessage(moReq.getMessage());
                    if(contentLog.getContentOrder() != 1) {
                        contentLog.setContentOrder(1);
                    }
                    contentLogService.update(contentLog);
                }
//                if (ptModel != null && ptModel.getCode() == 200) {
                if (postResult.getStatus().equalsIgnoreCase("0")) {
                    //goi Api lay Content tra ve sau khi update ngay sinh
                    //status GET_CONTENT
                    HandlingResult postResult1 = cpCvptService.notifyPTApi(moReq.getSmsId(), moReq.getFromNumber(), "", "", "GET_CONTENT", "");
                    LOG.info("RESPONSE_CONTENT::" + postResult1.parseResp());
                    if (postResult1.getStatus().equalsIgnoreCase("0")) {
                        PTModel ptModel1 = GSON_ALL.fromJson(postResult1.parseResp(), PTModel.class);
                        ptModel1.getData();
                        PTContentModel[] ptContentModel = ptModel1.getData();
                        content = ptContentModel[0].getContent();
                    }
                } else {
                    content = "";
                }
            } else {
                //mt NS sai dinh dang
                content = smsService.mtModelKeyCV().getMtNSErr();
            }
        } catch (Exception ex) {
            LOG.error("ERROR_builCVPTMessage", ex);
        }
        LOG.info("END::builCVPTMessage");
        return content;
    }

    private String builLessonMessage(MORequest moReq) {
        LOG.info("BEGIN::builLessonMessage");
        StringBuilder sb = new StringBuilder();
        String message = "";
        boolean isExistLesson = true;
        MTRequest mtReq = null;
        String mpin = Util.generateMPIN();
        try {
            String mess = moReq.getMessage().toUpperCase();
            String _lesson = mess.replace("MUA", "");
            String lesson = _lesson.trim();

            DateTime dtCurrent = new DateTime();
            Date registerDate = new Date();
            Date _dtCurrent = dtCurrent.toDate();
            Date _dtEndOfDay = Utils.getEndOfDay(dtCurrent);
            String expricedDate = new SimpleDateFormat("yyyyMMddHHmmss").format(_dtEndOfDay);

            //goi api dang ki mua le
            //status LESSON goi mua le
            HandlingResult postResult = cpCvptService.notifyPTApi(moReq.getSmsId(), moReq.getFromNumber(), "", expricedDate, "LESSON", lesson);
            LOG.info("RESPONSE_CVPT_LESSON::" + postResult.parseResp());
            //packageId 12 ma goi CV
            Subscriber subscriber = subscriberService.findByMsisdnAndPackageId(moReq.getFromNumber(), 12);

            //check truong hop khi cv dang active thi khong dang ky duoc goi mua le
            if (subscriber == null) {
                boolean isNew = false;
                BuySingleLog buySingleLog = buySingleLogService.findPackageSingleActive(moReq.getFromNumber(), lesson, registerDate);
                if (buySingleLog == null) {
                    isNew = true;
                    buySingleLog = new BuySingleLog();
                    buySingleLog.setMsisdn(moReq.getFromNumber());
                    buySingleLog.setPackageSingle(lesson);
                    buySingleLog.setRegisterDate(_dtCurrent);
                    buySingleLog.setExpiredDate(_dtEndOfDay);
                    buySingleLog.setStatus(1);
                    buySingleLog.setMoSms(moReq.getMessage());
                }
                if (isNew) {
                    buySingleLogService.create(buySingleLog);
                    isExistLesson = false;
                }
            } else {
                if (subscriber.getStatus() != 1) {
                    BuySingleLog buySingleLog = buySingleLogService.findPackageSingleActive(moReq.getFromNumber(), lesson, registerDate);
                    //log bai hoc cua thue bao dang ki
                    if (buySingleLog == null) {
                        BuySingleLog buySingleLog1 = new BuySingleLog();
                        buySingleLog1.setMsisdn(moReq.getFromNumber());
                        buySingleLog1.setPackageSingle(lesson);
                        buySingleLog1.setRegisterDate(_dtCurrent);
                        buySingleLog1.setExpiredDate(_dtEndOfDay);
                        buySingleLog1.setStatus(1);
                        buySingleLog1.setMoSms(moReq.getMessage());
                        buySingleLogService.create(buySingleLog1);
                        isExistLesson = false;
                    }
                }
            }


            //Kiem tra thue bao co active goi CV hay ko
            if (subscriber != null && subscriber.getStatus() == 1) {
                //mua le khong thanh cong khi cv dang active
                message = smsService.mtModelKeyCV().getMtMUAErrCvActive();
            } else if (isExistLesson || postResult.getStatus().equalsIgnoreCase("410")) {
                //mua le khong thanh cong khi bai hoc dang active
                LOG.info("PARAM::isExistLesson=" + isExistLesson + ";postResult=" + postResult.getStatus());
                message = smsService.mtModelKeyCV().getMtMUAError();
                message = message.replaceAll("<TENNOIDUNG>", lesson);
            } else if (!isExistLesson) {
                //Neu ma chua mua bai hoc thi thuc hien CHARGE va gui MT
                String regex = "([a-zA-Z]){2,4}\\d\\d";
                boolean checkLessonCode = lesson.matches(regex);
                if (!checkLessonCode) {
                    message = smsService.mtModelKeyCV().getMtSmsCodeErr();
                } else if (postResult.getStatus().equalsIgnoreCase("422")) {//lesson bi sai ma
                    message = smsService.mtModelKeyCV().getMtSmsCodeErr();
                } else if (!postResult.getStatus().equalsIgnoreCase("0")) {//Goi API ko thanh cong
                    message = "";
                } else {
                    //Goi API thanh cong
                    String result = buyLessonPackage(moReq, _dtCurrent, mpin);//CHARGE thanh cong
                    if ("1".equals(result) || "OK".equals(result)) {
                        //GUI MT
                        if (subscriber != null && subscriber.getStatus() == 0) {
                            message = smsService.mtModelKeyCV().getMtMUASucces();
                            message = message.replaceAll("<TENNOIDUNG>", lesson);
                        } else if (subscriber == null) {
                            //goi api reset mat khau
                            HandlingResult postResult1 = cpCvptService.notifyPTApi(moReq.getSmsId(), moReq.getFromNumber(), "", "", "RESET_PASS", "");
                            LOG.info("RESPONSE_CVPT_RESET_PASS::" + postResult1.parseResp());
                            if (postResult1.getStatus().equalsIgnoreCase("0")) {
                                CVPTModel ptModel1 = GSON_ALL.fromJson(postResult1.parseResp(), CVPTModel.class);
                                PTRegDataModel ptRegDataModel = ptModel1.getData();
                                mpin = ptRegDataModel.getPassword();
                            }
                            //gui mt dang ky mua goi cuoc thanh cong
                            message = smsService.mtModelKeyCV().getMtMUASucces();
                            message = message.replaceAll("<TENNOIDUNG>", lesson);
                            mtReq = MessageBuilder.buildMTRequest(moReq.getToNumber(), moReq.getFromNumber(), message, moReq, moReq.getCommand());
                            if (mtReq != null) {
                                mtQueueToCSP.offer(mtReq);
                                synchronized (mtQueueToCSPNotifier) {
                                    mtQueueToCSPNotifier.notifyAll();
                                }
                            }
                            //mt mat khau app va huong dan download ung dung
                            message = smsService.mtModelKeyCV().getMtMKApp();
                            message = message.replaceAll("<MATKHAU>", mpin);
                        }
                    } else {
                        //GUI MT ko charge thanh cong
                        message = smsService.mtModelKeyCV().getMtChargeErr();
                    }

                }
            }
            sb.append(message);


        } catch (Exception ex) {
            LOG.error("", ex);
        }
        LOG.info("END::builLessonMessage");
        return sb.toString();
    }

    private String builChangePassMessage(MORequest moReq) {
        LOG.info("BEGIN::builChangePassMessage");
        String mpin = Util.generateMPIN();
        StringBuilder sb = new StringBuilder();
        String message = "";
        try {
            //goi api reset mat khau
            HandlingResult postResult = cpCvptService.notifyPTApi(moReq.getSmsId(), moReq.getFromNumber(), "", "", "RESET_PASS", "");
            LOG.info("RESPONSE_CVPT_RESET_PASS::" + postResult.parseResp());
            if (postResult.getStatus().equalsIgnoreCase("0")) {
                CVPTModel ptModel1 = GSON_ALL.fromJson(postResult.parseResp(), CVPTModel.class);
                PTRegDataModel ptRegDataModel = ptModel1.getData();
                mpin = ptRegDataModel.getPassword();
            }
            //lay thue bao sub dang active
            List<Subscriber> listSub = subscriberService.findActiveByMsisdn(moReq.getFromNumber());
            SysParam sysParamSMS = sysParamService.findByKey("MT_KEY_CV");
            if (sysParamSMS != null) {
                mtModel = GSON_ALL.fromJson(sysParamSMS.getValue(), MtModel.class);
                if (listSub != null && !listSub.isEmpty()) {
                    for (Subscriber list : listSub) {
                        //thay doi ma pin
                        list.setMpin(mpin);
                        subscriberService.update(list);
                    }
                    //mt mat khau moi khi dang ky dich vu
                    message = mtModel.getMtGetMK().replaceFirst("<MATKHAU>", mpin);
                    sb.append(message);
                } else {
                    //mt mat khau moi khi chua dang ky dich vu
                    message = mtModel.getMtErrGetMK();
                    sb.append(message);
                }
            }
        } catch (Exception ex) {
            LOG.error("", ex);
        }
        LOG.info("END::builChangePassMessage");
        return sb.toString();
    }

    private String buyLessonPackage(MORequest moReq, Date chargeDate, String mpin) {
        LOG.info("buyLessonPackage: BEGIN -----");
        String result = "0";
        ChargeLog chargeLog = new ChargeLog();
        SubsChargeLog subsChargeLog = new SubsChargeLog();
        try {
            String content_ID = "0000000010";
            String category_ID = "000010";
            String spId = "001";
            String cpId = "001";
            result = chargingCSPClient.minusMoneyCheckMO(AppParams.SHORT_CODE,
                    moReq.getFromNumber(), moReq.getSmsId(),
                    "CV", "CV",
                    spId, cpId, content_ID, category_ID, "1000");
            LOG.error("processBuyLessonPackage.result=", result);
            //Ghi log charge goi mua le cua KH
            chargeLog.setAmount(1000);
            chargeLog.setMsisdn(moReq.getFromNumber());
            chargeLog.setCallStatus(true);
            chargeLog.setResultStatus("1".equals(result) || "OK".equals(result) ? true : false);
            chargeLog.setType("MUA");
            chargeLog.setTransDate(new Date());
            chargeLogService.create(chargeLog);
            //Ghi log subs charge mua le cua KH
            subsChargeLog.setPriceCharge(1000);
            subsChargeLog.setMsisdn(moReq.getFromNumber());
            subsChargeLog.setStatus("1".equals(result) || "OK".equals(result) ? 1 : 0);
            subsChargeLog.setType("MUA");
            subsChargeLog.setOrgRequest(moReq.getMessage());
            subsChargeLog.setChargeDate(chargeDate);
            subsChargeLog.setPackageName("CV");
            subsChargeLogService.create(subsChargeLog);

            ContentLog contentLog = contentLogService.findByMsisdn(moReq.getFromNumber());
            //ghi log vao bang content_log
            if (contentLog == null) {
                contentLog = new ContentLog();
                contentLog.setMsisdn(moReq.getFromNumber());
                contentLog.setPackageCode("CV");
                contentLog.setOrgMessage(moReq.getMessage());
                contentLog.setMpin(mpin);
                contentLogService.create(contentLog);
            } else {
                contentLog.setPackageCode("CV");
                contentLog.setOrgMessage(moReq.getMessage());
                contentLog.setMpin(mpin);
                contentLogService.update(contentLog);
            }
        } catch (Exception ex) {
            LOG.error("processBuyLessonPackage: ERROR", ex);
            //GUI MT thong bao he thong loi
            MTRequest mtReqPrivate = MessageBuilder.buildMTRequest(AppParams.SHORT_CODE, strPrivateNumber,
                    "ERR_BUY_LESSON " + ex.getMessage(), null, "ERR_BUY_LESSON");
            if (mtReqPrivate != null) {
                mtQueueToCSP.offer(mtReqPrivate);
                synchronized (mtQueueToCSPNotifier) {
                    mtQueueToCSPNotifier.notifyAll();
                }
            }
        }
        LOG.info("buyLessonPackage: END -----");
        return result;
    }

//	private String builCVPTMessage(MORequest moReq) {
//		LOG.info("BEGIN::builCVPTMessage");
//		String content = "";
//		try {
//
//				HandlingResult postResult1 = cpCvptService.notifyPTApi(moReq.getSmsId(), moReq.getFromNumber(),"", "", "GET_CONTENT");
//			String test = postResult1.parseResp();
//			LOG.info(test);
//					PTModel ptModel1 = new PTModel();
//					ptModel1 = GSON_ALL.fromJson(postResult1.parseResp(), PTModel.class);
//
//					ptModel1.getData();
//					PTContentModel[] ptContentModel = ptModel1.getData();
//					content = ptContentModel[0].getContent();
//
//		} catch (Exception ex) {
//			LOG.error("ERROR_builCVPTMessage", ex);
//		}
//		LOG.info("END::builCVPTMessage");
//		return content;
//	}

//    public static void main(String[] agrv) throws InvalidKeyException, NoSuchAlgorithmException {
////        YomiPhongThuy yomiPhongThuy = new YomiPhongThuy();
////        yomiPhongThuy.getContent("0936361410", "GET_CONTENT");
//        //yomiPhongThuy.test("40/02/1999");
//        DateValidator dateValidator = new DateValidator();
//        boolean check = dateValidator.validate("02-02-1999");
//    }

}
