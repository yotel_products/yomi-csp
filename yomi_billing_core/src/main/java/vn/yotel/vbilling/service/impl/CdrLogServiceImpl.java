package vn.yotel.vbilling.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.yotel.commons.bo.impl.GenericBoImpl;
import vn.yotel.vbilling.jpa.CdrLog;
import vn.yotel.vbilling.model.CdrLogModel;
import vn.yotel.vbilling.repository.CdrLogRepo;
import vn.yotel.vbilling.repository.ReportCommonRepo;
import vn.yotel.vbilling.service.CdrLogService;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service(value = "cdrLogService")
@Transactional
public class CdrLogServiceImpl extends GenericBoImpl<CdrLog, Long> implements CdrLogService {
    @Resource
    ReportCommonRepo reportCommonRepo;

    @Resource
    CdrLogRepo cdrLogRepo;

    private Logger LOG = LoggerFactory.getLogger(CdrLogServiceImpl.class);

    @SuppressWarnings("unchecked")
    @Override
    public CdrLogRepo getDAO() {
        return this.cdrLogRepo;
    }

    @Override
    public List<CdrLogModel> reportControlMonth(Date fromDate, Date toDate) {
        List<Object[]> listObj = reportCommonRepo.reportControlMonth(fromDate, toDate);
        //List<Object[]> listObj = cdrLogRepo.reportControlMonth(fromDate, toDate);
        List<CdrLogModel> listCdrLog = new ArrayList<>();
        for (Object[] list : listObj) {
            CdrLogModel cdr = new CdrLogModel();
            cdr.setAmount(String.valueOf(list[0]));
            cdr.setContentId(String.valueOf(list[1]));
            cdr.setNumSub(String.valueOf(list[2]));
            cdr.setTotalAmount(String.valueOf(list[3]));
            listCdrLog.add(cdr);
        }
        return listCdrLog;
    }

    @Override
    public List<CdrLogModel> reportRevenueMonthly(Date fromDate, Date toDate) {
        List<CdrLogModel> listCdrLog = new ArrayList<>();
        try {
            List<Object[]> listObj = reportCommonRepo.reportRevenueMonthly(fromDate, toDate);
            for (Object[] list : listObj) {
                CdrLogModel cdr = new CdrLogModel();
                cdr.setDate1(String.valueOf(list[0]));
                cdr.setTotalAmount(String.valueOf(list[1]));
                listCdrLog.add(cdr);
            }
        } catch (Exception e) {
            LOG.error("", e);
        }
        return listCdrLog;
    }

    @Override
    public List<CdrLog> findCdrLogByDate(Date fromDate, Date toDate) {
        return cdrLogRepo.findCdrLogByDate(fromDate, toDate);
    }

    @Override
    public List<CdrLog> findCdrChargeAmount(Date fromDate, Date toDate, String msisdn, String type) {
        return cdrLogRepo.findCdrChargeAmount(fromDate, toDate, msisdn, type);
    }


}
