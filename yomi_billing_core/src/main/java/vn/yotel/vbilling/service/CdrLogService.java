package vn.yotel.vbilling.service;

import vn.yotel.commons.bo.GenericBo;
import vn.yotel.vbilling.jpa.CdrLog;
import vn.yotel.vbilling.model.CdrLogModel;

import java.util.Date;
import java.util.List;

public interface CdrLogService extends GenericBo<CdrLog, Long> {
	List<CdrLogModel> reportControlMonth(Date fromDate, Date toDate);
	List<CdrLogModel> reportRevenueMonthly(Date fromDate, Date toDate);
	List<CdrLog> findCdrLogByDate(Date fromDate, Date toDate);
	List<CdrLog> findCdrChargeAmount(Date fromDate, Date toDate, String msisdn, String type);
}
