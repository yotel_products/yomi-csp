package vn.yotel.vbilling.model;

public class MtModel {
	
	private String mtHD;
	private String mtHDXS;
	private String mtHDDT;
	private String mtKT;
	private String mtNonSubsKT;
	private String mtMK;
	private String mtNonSubsMK;
	private String mtInvalid;
	private String mtSystemError;
	private String mtSmsFogotMessage;
	private String mtNS;
	private String mtGetMK;
	private String mtErrGetMK;
	private String mtMUASucces;
	private String mtMUAError;
	private String mtChargeErr;
	private String mtSmsCodeErr;
	private String mtNSSucces;
	private String mtNSErr;
	private String mtMKApp;
	private String mtMUAErrCvActive;
	private String mtND;
	private String mtNDNSNull;
	
	public String getMtHD() {
		return mtHD;
	}
	public void setMtHD(String mtHD) {
		this.mtHD = mtHD;
	}
	public String getMtHDXS() {
		return mtHDXS;
	}
	public void setMtHDXS(String mtHDXS) {
		this.mtHDXS = mtHDXS;
	}
	public String getMtHDDT() {
		return mtHDDT;
	}
	public void setMtHDDT(String mtHDDT) {
		this.mtHDDT = mtHDDT;
	}
	public String getMtKT() {
		return mtKT;
	}
	public void setMtKT(String mtKT) {
		this.mtKT = mtKT;
	}
	public String getMtMK() {
		return mtMK;
	}
	public void setMtMK(String mtMK) {
		this.mtMK = mtMK;
	}
	public String getMtInvalid() {
		return mtInvalid;
	}
	public void setMtInvalid(String mtInvalid) {
		this.mtInvalid = mtInvalid;
	}
	public String getMtSystemError() {
		return mtSystemError;
	}
	public void setMtSystemError(String mtSystemError) {
		this.mtSystemError = mtSystemError;
	}
	public String getMtNonSubsMK() {
		return mtNonSubsMK;
	}
	public void setMtNonSubsMK(String mtNonSubsMK) {
		this.mtNonSubsMK = mtNonSubsMK;
	}
	public String getMtNonSubsKT() {
		return mtNonSubsKT;
	}
	public void setMtNonSubsKT(String mtNonSubsKT) {
		this.mtNonSubsKT = mtNonSubsKT;
	}
	public String getMtSmsFogotMessage() {
		return mtSmsFogotMessage;
	}
	public void setMtSmsFogotMessage(String mtSmsFogotMessage) {
		this.mtSmsFogotMessage = mtSmsFogotMessage;
	}
	public String getMtNS() {
		return mtNS;
	}
	public void setMtNS(String mtNS) {
		this.mtNS = mtNS;
	}
	public String getMtGetMK() {
		return mtGetMK;
	}
	public void setMtGetMK(String mtGetMK) {
		this.mtGetMK = mtGetMK;
	}
	public String getMtErrGetMK() {
		return mtErrGetMK;
	}
	public void setMtErrGetMK(String mtErrGetMK) {
		this.mtErrGetMK = mtErrGetMK;
	}
	public String getMtMUASucces() {
		return mtMUASucces;
	}
	public void setMtMUASucces(String mtMUASucces) {
		this.mtMUASucces = mtMUASucces;
	}
	public String getMtMUAError() {
		return mtMUAError;
	}
	public void setMtMUAError(String mtMUAError) {
		this.mtMUAError = mtMUAError;
	}
	public String getMtChargeErr() {
		return mtChargeErr;
	}
	public void setMtChargeErr(String mtChargeErr) {
		this.mtChargeErr = mtChargeErr;
	}
	public String getMtSmsCodeErr() {
		return mtSmsCodeErr;
	}
	public void setMtSmsCodeErr(String mtSmsCodeErr) {
		this.mtSmsCodeErr = mtSmsCodeErr;
	}

    public String getMtNSSucces() {
        return mtNSSucces;
    }

    public void setMtNSSucces(String mtNSSucces) {
        this.mtNSSucces = mtNSSucces;
    }

    public String getMtNSErr() {
        return mtNSErr;
    }

    public void setMtNSErr(String mtNSErr) {
        this.mtNSErr = mtNSErr;
    }

	public String getMtMKApp() {
		return mtMKApp;
	}

	public void setMtMKApp(String mtMKApp) {
		this.mtMKApp = mtMKApp;
	}

	public String getMtMUAErrCvActive() {
		return mtMUAErrCvActive;
	}

	public void setMtMUAErrCvActive(String mtMUAErrCvActive) {
		this.mtMUAErrCvActive = mtMUAErrCvActive;
	}

	public String getMtND() {
		return mtND;
	}

	public void setMtND(String mtND) {
		this.mtND = mtND;
	}

	public String getMtNDNSNull() {
		return mtNDNSNull;
	}

	public void setMtNDNSNull(String mtNDNSNull) {
		this.mtNDNSNull = mtNDNSNull;
	}
}
