package vn.yotel.vbilling.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import vn.yotel.vbilling.jpa.Subscriber;

@Repository
public interface SubscriberRepo extends JpaRepository<Subscriber, Integer> {

	Subscriber findByMsisdn(@Param("msisdn") String msisdn);

	@Query(value = "SELECT subs FROM Subscriber subs WHERE subs.msisdn = :msisdn ")
	List<Subscriber> findAllByMsisdn(@Param("msisdn") String msisdn);

	@Query(value = "SELECT subs FROM Subscriber subs WHERE subs.msisdn = :msisdn AND subs.status = 1")
	List<Subscriber> findActiveByMsisdn(@Param("msisdn") String msisdn);

	Subscriber findByMsisdnAndPackageIdAndStatus(String msisdn, Integer packageId, int status);

	Subscriber findByMsisdnAndPackageId(String msisdn, Integer packageId);

	@Query(value = "SELECT subs FROM Subscriber subs WHERE subs.msisdn = :msisdn AND subs.mpin = :mpin ")
	List<Subscriber> findAllByMisdnAndMpin(@Param("msisdn") String msisdn, @Param("mpin") String mpin);

	@Query(value = "SELECT a.msisdn, a.status FROM core_subscriber a\n" +
			"WHERE a.status = 1 AND a.package_id = :packageId\n" +
			"AND a.register_date >= DATE_ADD(CURRENT_DATE, INTERVAL -1 DAY) ", nativeQuery = true)
	List<Object[]> findAllSubsActiveByPackageId(@Param("packageId") Integer packageId);

	@Query(value = "SELECT a.msisdn, a.product_id, a.package_id, a.status, a.register_date " +
			"FROM core_subscriber a WHERE a.status = 1 " +
			"AND a.register_date >= DATE_ADD(CURRENT_DATE, INTERVAL -45 DAY) ", nativeQuery = true)
	List<Object[]> findAllSubsActive();

	@Query(value = "SELECT DATE(a.register_date), COUNT(1) FROM core_subscriber a \n" +
			"WHERE a.status = 1 AND a.package_id = 12 ", nativeQuery = true)
	List<Object[]> countSubsActiveCV();
}
