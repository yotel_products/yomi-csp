package vn.yotel.vbilling.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import vn.yotel.vbilling.jpa.CdrLog;

import java.util.Date;
import java.util.List;
import java.util.Objects;

public interface CdrLogRepo extends JpaRepository<CdrLog, Long> {
	@Query(value = "SELECT content_id, COUNT(1), SUM(amount) FROM cdr_log \n" +
			"WHERE 1 AND time1 >= :fromDate \n" +
			"AND time1 < DATE_ADD(:toDate, INTERVAL 1 DAY) AND status = 1 \n" +
			"GROUP BY content_id", nativeQuery = true)
	List<Object[]> reportControlMonth(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

	@Query(value = "SELECT date_format(time1,'%Y/%m/%d') date1, sum(amount) FROM cdr_log " +
			"WHERE 1 and time1 >= :fromDate " +
			"and time1 < :toDate group by date_format(time1,'%Y/%m/%d') ", nativeQuery = true)
	List<Object[]> reportTotalMoneyByDate(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

	@Query(value = "SELECT cdr FROM CdrLog cdr WHERE cdr.time1 >= :fromDate AND cdr.time1 < :toDate AND cdr.status = 1 ", nativeQuery = false)
	List<CdrLog> findCdrLogByDate(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

	@Query(value = "SELECT cdr FROM CdrLog cdr WHERE cdr.msisdn = :msisdn AND (cdr.type = :type OR :type = '') " +
			"AND cdr.time1 >= :fromDate AND cdr.time1 < :toDate " +
			"AND cdr.status = 1 ")
	List<CdrLog> findCdrChargeAmount(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("msisdn") String msisdn, @Param("type") String type);
}
